import { Injectable } from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation/ngx';

@Injectable({
  providedIn: 'root'
})
export class LocationService {

  constructor(private geoLocation: Geolocation) { }


  public getLocation() {
    console.log("Service");
    return this.geoLocation.getCurrentPosition();
  }

  public watchPosition() {
    return this.geoLocation.watchPosition();
  }
}
