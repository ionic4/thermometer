import { Component, ViewChild, Renderer, OnInit, AfterViewInit } from '@angular/core';
import { Coordinates } from '@ionic-native/geolocation/ngx';
import { LocationService } from '../location.service';
import { WeatherService } from '../weather.service';
import { Chart } from 'chart.js';
import { Platform } from '@ionic/angular';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit, AfterViewInit {
  coordinates: Coordinates;
  temp: any;
  @ViewChild('thermometer', null) thermometer;
  canvasElement: any;
  listener: any;
  loading: any;

  constructor(private locationService: LocationService,
              private weatherService: WeatherService,
              public renderer: Renderer,
              public platform: Platform,
              public loadingController: LoadingController) {}

  ngOnInit() {
    this.createLoading();
    this.locationService.getLocation().then(data => {
      this.coordinates = data.coords;
      this.weatherService.getWeather(this.coordinates.latitude.toString(), 
      this.coordinates.longitude.toString()).then(response => {
        const weatherdata = JSON.parse(JSON.stringify(response));
        this.temp = weatherdata.main.temp;
        this.drawThermometer();
        this.loading.dismiss();
      }).catch ((error) => {
        this.loading.dismiss();
        this.temp = error.message;
      });
    }).catch((error) => {
      this.loading.dismiss();
      this.temp = error.message;
    });
  }

  async createLoading() {
    this.loading = await this.loadingController.create({
      message: 'Loading weather'
    });
    await this.loading.present();
  }

  ngAfterViewInit() {
    this.scaleCanvas();
    this.listener = window.addEventListener('resize', () => { this.scaleCanvas(); });
  }

  ionViewWillLeave() {
    window.removeEventListener('resize', this.listener);
  }

  scaleCanvas() {
    this.canvasElement = this.thermometer.nativeElement;

    this.renderer.setElementAttribute(this.canvasElement, 'width', this.platform.width() + '');
    this.renderer.setElementAttribute(this.canvasElement, 'height', this.platform.height() + '');
  }

  drawThermometer() {
    let color = 'rgba(255, 99, 132, 0.2)';

    if (this.temp < 0) {
      color = 'rgba(54, 162, 235, 0.2)';
    }

    // tslint:disable-next-line: no-unused-expression
    new Chart(this.thermometer.nativeElement, {
      type: 'bar',
      data: {
          datasets: [{
              data: [this.temp],
              backgroundColor: [
                  color
              ],
              borderColor: [
                  color
              ],
              borderWidth: 1
          }]
      },
      options: {
          scales: {
              yAxes: [{
                  ticks: {
                      min:-40,
                      max: 40,
                      stepSize: 5
                  }
              }]
          },
          legend: {
            display: false
          }
      }
    });
  }
}
